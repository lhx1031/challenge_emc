import time
from novaclient import client
from novaclient.exceptions import NotFound

from docker import Client


class NovaCapacityManager(object):
    NOVA_CLIENT_VERSION = "2"
    DOCKER_DAEMON_PORT = "2375"
    DOCKER_DAEMON_BASE_URL = "tcp://%s:%s"

    def __init__(self, username, password, auth_url, project_id):
        """initialize the credential dict"""
        self.nova_creds = {
            "username": username,
            "api_key": password,
            "auth_url": auth_url,
            "project_id": project_id,
            "version": NovaCapacityManager.NOVA_CLIENT_VERSION
        }

        # instantiating a novaclient.Client object
        self.nova = client.Client(**self.nova_creds)

    def vm_start(self, vm_name, image_name, flavor_name):
        """start an instance"""
        try:
            image = self.nova.images.find(name=image_name)
        except NotFound:
            raise NotFound(image_name)

        try:
            flavor = self.nova.flavors.find(name=flavor_name)
        except NotFound:
            raise NotFound(flavor_name)

        try:
            instance = self.nova.servers.create(name=vm_name, image=image, flavor=flavor)
            # return the instance object created
            return instance
        except:
            raise Exception("Failed to create instance, vm_name=%s, image=%s, flavor=%s"
                            % (vm_name, image_name, flavor_name))

    def start_container_on_vm(self, instance, container_name, docker_image_id, command=""):
        """start a docker on a vm instance"""
        # check if instance object is None
        if not instance:
            raise Exception("instance object is None")

        try:
            # get the newest instance object info (status of an instance may change since the method is called)
            instance = self.nova.servers.get(instance.id)
        except:
            raise NotFound(instance.name)

        if instance.status != "ACTIVE":
            raise Exception("Instance=%s in not active" % instance.name)

        # get the instance floating ip
        instance_addresses = instance.addresses

        docker_daemon_base_url = ""

        for ip_address in instance_addresses.get("private", []):
            if ip_address["OS-EXT-IPS:type"] == "fixed":
                docker_daemon_base_url = NovaCapacityManager.DOCKER_DAEMON_BASE_URL % (ip_address["addr"],
                                                                                       NovaCapacityManager.DOCKER_DAEMON_PORT)

        if not docker_daemon_base_url:
            raise Exception("Failed to get docker daemon base url on instance=%s" % instance.name)

        # start container on the vm
        try:
            docker_cli = Client(base_url=docker_daemon_base_url)
            container = docker_cli.create_container(name=container_name, image=docker_image_id, command=command)
            docker_cli.start(container=container_name)
        except Exception as e:
            raise Exception("Failed to start container=%s on vm=%s, error=%s" % (container_name, instance.name, str(e)))

        return container

    def vm_wait(self, timeout, instance_list):
        """wait on vm state"""
        time_start = time.time()
        # check the updated status of the instances and delete the Active instances
        while instance_list:
            instance_list = [instance for instance in instance_list if
                             self.nova.servers.get(instance.id).status != "ACTIVE"]
            time.sleep(5)
            if (time.time() - time_start) >= timeout:
                raise Exception("Wait for instance time out")


if __name__ == "__main__":
    test_username = "admin",
    test_password = "nomoresecrete"
    test_auth_url = "http://192.168.252.145:5000/v2.0"
    test_project_id = "admin"
    nova_capacity_manager = NovaCapacityManager(test_username, test_password, test_auth_url, test_project_id)

    test_vm_name = "test_vm"
    test_image_name = "cirros-0.3.4-x86_64-uec"
    test_flavor_name = "m1.tiny"

