- Your level of familiarity with python, git, OpenStack, and Docker.
Python and git: I use python and git daily at work.

Openstack: Familiar. When we were investigating openstack and I created a small openstack plugin for our cloud platform. Currently we mainly use linux container. 

Docker: I am new to Docker, this is the first time I use it.


- How long it took you to complete:

Around 3.5 hours, not including the time to download the ubuntu images and install them.


- A short description of how you developed the solution:

- Install OpenStack Devstack (take a screenshot to show completion).

1.follow the instructions here: http://docs.openstack.org/developer/devstack/guides/single-machine.html
2. download devstack onto the ubuntu machine
3. create a local.conf file under /devstack
4. run ./stack.sh to start openstack and wait for the installation/initialization to complete

- Create a python script which will launch virtual machines.
1. create the python script NovaCapacityManager.py to launch vm and test(put the simple test in main)

- Create a python script, or modify the existing python script to run a docker container inside a virtual machine.
1. read docker documentation
2. create a draft script to create container on vm

- Be able to spin up a docker container in the virtual machine running on Devstack.
1. in order to run docker,  upload a QCOW2 format ubuntu cloud image to openstack and create an ubuntu instance
2. install docker
3. modify /etc/default/docker to run docker daemon on  tcp://0.0.0.0:2375 DOCKER_OPTS="-H tcp://0.0.0.0:2375"
4. modify the draft script to create container on vm and add method in NovaCapacityManager to create container on vm
          

- A description of the known problems you found but ran out of time to fix, and how you could fix them given more time:

As I have several other commitments so I have limited time working on the challenge. There are a lot we can improve, such as:

1. In order to be able to create dockers inside vms on devstack, we could have docker preinstalled and docker deamon properly configed in the instance image. In that case whenever we boot up an vm instance, the docker daemon is ready to help us manage the docker containers. The image may also include the docker image we are going to run.

2. When I set the docker daemon to host on 0.0.0.0 it gave me security warning so we can change the setting to only receiving requests from specific ips to improve the security.

3. We can add an api server on the compute node to handle the grow vm/contaienr requests and make call to the NovaCapacityManager under the cover to have a better interface.

4. In my design the NovaCapacityManager will be running as an instance on the compute node to grow new vms and grow containers inside those vms, another design we can go with will be create an additional ContainerCapacityManager and run it together with an api server on the vms we boot up, then we can send out the request through the rest api we design for the container management. This will make our design more flexible, if we decide to use LXC driver(or any other technologies) instead of Docker to manager our containers in the future, we don’t have to change the implementation on our NovaCapacityManager as we keep the interface. Just need to implement different ContainerCapacityManagers base on the technology we are using.

and so on...



